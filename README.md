# IRA Matrix Process Portal

This portal will bring clarity to all the matrix processes

## TODO

polish the CORS and security part of calling activi apis in the matrix process. In order to be able to call use the following `WebSecurityConfig`

```
package com.iralogix.recordkeeper.matrix.process;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@EnableWebSecurity
@Order(SecurityProperties.DEFAULT_FILTER_ORDER)
@EnableGlobalMethodSecurity(jsr250Enabled = true, securedEnabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static String REALM = "MatrixProcess";

    @Autowired
    private MatrixIntegrationConfiguration configuration;

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        // prepare delegating password encoder
        // https://www.baeldung.com/spring-security-5-default-password-encoder
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        // set BASIC authentication username/password
        auth.inMemoryAuthentication()
            .withUser(configuration.getActivitiRestUsername())
            .password(encoder.encode(configuration.getActivitiRestPassword()))
            .roles("ACTUATOR");
    }

    @Override
    public void configure(WebSecurity http) throws Exception {
        // ignore /health from spring security
        http.ignoring()
            .antMatchers(HttpMethod.GET, "/actuator/health");
        // use base implementation
        super.configure(http);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // configure http
        http.cors().and()
            .csrf().disable()
            .httpBasic().authenticationEntryPoint(authenticationEntryPoint());
        // authorization
        http.authorizeRequests()
            .antMatchers("/**").permitAll();
    }

    @Bean
    public BasicAuthenticationEntryPoint authenticationEntryPoint() {
        // create entry point
        BasicAuthenticationEntryPoint entryPoint = new BasicAuthenticationEntryPoint();
        // set realm
        entryPoint.setRealmName(REALM);
        // return service instance
        return entryPoint;
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");
            }
        };
    }
}

```
