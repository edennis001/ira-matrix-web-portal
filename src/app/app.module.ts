import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { TypeaheadModule } from "ngx-bootstrap";

import { MainComponent } from "./main/main.component";
import { AppService } from "./app.service";
import { SmartTableComponent } from "./smart-table/smart-table.component";
import { AppComponent } from "./app.component";
import { TableModule } from "primeng/table";
import { ChartModule } from "primeng/chart";
import { VisualsComponent } from "./visuals/visuals.component";

const routes: Routes = [
  { path: "main", component: MainComponent },
  { path: "smart-table", component: SmartTableComponent },
  { path: "visuals", component: VisualsComponent },
];
@NgModule({
  declarations: [
    AppComponent,
    SmartTableComponent,
    MainComponent,
    VisualsComponent,
  ],
  imports: [
    TableModule,
    ChartModule,
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    BrowserAnimationsModule,
    TypeaheadModule.forRoot(),
  ],
  providers: [AppService],
  bootstrap: [AppComponent],
})
export class AppModule {}
