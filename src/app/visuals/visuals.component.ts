import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-visuals",
  templateUrl: "./visuals.component.html",
  styleUrls: ["./visuals.component.css"],
})
export class VisualsComponent implements OnInit {
  data: any;

  constructor() {}

  ngOnInit() {
    this.data = {
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets: [
        {
          label: "First Dataset",
          data: [65, 59, 80, 81, 56, 55, 40],
        },
        {
          label: "Second Dataset",
          data: [28, 48, 40, 19, 86, 27, 90],
        },
      ],
    };
  }
}
