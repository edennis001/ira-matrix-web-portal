import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "./../environments/environment";

@Injectable()
export class AppService {
  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: "Basic " + btoa("admin:admin"),
    }),
  };

  constructor(private http: HttpClient) {}

  getMatrixDefinitions() {
    return this.http.get(
      `http://${environment.matrixUrl}/repository/process-definitions?size=10000`,
      this.httpOptions
    );
  }
  // &size=15&sort=id&start=15
  getProcessInstances(key, size, start) {
    let url = `http://${environment.matrixUrl}/runtime/process-instances?size=${size}&start=${start}`;
    if (key) {
      url += `&processDefinitionKey=${key}`;
    }
    return this.http.get(url, this.httpOptions);
  }

  // NOT WORKING
  retryInstance(id, instance) {
    return this.http.put(
      `http://${environment.matrixUrl}/runtime/executions/${id}`,
      instance,
      this.httpOptions
    );
  }

  deleteInstance(id) {
    return this.http.delete(
      `http://${environment.matrixUrl}/runtime/process-instances/${id}`,
      this.httpOptions
    );
  }
}
