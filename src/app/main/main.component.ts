import { Component } from "@angular/core";
import { AppService } from "../app.service";
import { TypeaheadMatch } from "ngx-bootstrap/typeahead/typeahead-match.class";

@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.css"],
})
export class MainComponent {
  title = "Matrix Process Portal";
  definitions;
  instances: any = { data: [] };
  selectedValue: string;
  selectedOption = { key: null };
  startPage = 0;
  pageSize = 30;

  constructor(private appService: AppService) {}

  ngOnInit() {
    this.appService.getMatrixDefinitions().subscribe((res) => {
      this.definitions = res["data"];
    });

    this.getInstances();
  }

  onSelect(event: TypeaheadMatch): void {
    this.selectedOption = event.item;
  }

  search() {
    if (!this.selectedValue) {
      this.selectedOption = { key: null };
    }
    this.getInstances();
  }

  prev() {
    if (this.startPage > 0) {
      this.startPage -= this.pageSize;
      this.getInstances();
    }
  }

  next() {
    console.log(this.instances);
    if (this.startPage < this.instances["total"]) {
      this.startPage += this.pageSize;
      this.getInstances();
    }
  }

  retryInstance(id, instance) {
    this.appService.retryInstance(id, instance).subscribe((res) => {
      this.getInstances();
    });
  }

  deleteInstance(id) {
    this.appService.deleteInstance(id).subscribe((res) => {
      this.getInstances();
    });
  }

  private getInstances() {
    this.appService
      .getProcessInstances(
        this.selectedOption.key,
        this.pageSize,
        this.startPage
      )
      .subscribe((res) => {
        this.instances = res;
      });
  }
}
