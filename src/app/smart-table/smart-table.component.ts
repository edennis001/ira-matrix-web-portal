import { Component, OnInit } from "@angular/core";
import { AppService } from "../app.service";

@Component({
  selector: "app-smart-table",
  templateUrl: "./smart-table.component.html",
  styleUrls: ["./smart-table.component.css"],
})
export class SmartTableComponent implements OnInit {
  instances: [];
  loading = true;

  constructor(private appService: AppService) {}

  ngOnInit() {
    this.getInstances();
  }

  private getInstances() {
    this.appService.getProcessInstances(null, 10000, 0).subscribe((res) => {
      this.instances = res["data"];
      this.loading = false;
    });
  }
}
