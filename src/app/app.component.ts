import { Component } from "@angular/core";
import { AppService } from "./app.service";
import { TypeaheadMatch } from "ngx-bootstrap/typeahead/typeahead-match.class";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  title = "Matrix Process Portal";
  definitions;
  instances: any = { data: [] };
  selectedValue: string;
  selectedOption = { key: null };
  startPage = 0;
  pageSize = 30;

  constructor(private appService: AppService) {}
}
